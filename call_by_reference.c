#include<stdio.h>

void fun(int *p)
{
    // printf("Value of p: %p\n",p);
    // printf("Main Value of p: %d\n",*p);
    *p=600; //dereference of Address
}

int main (){
    int x=10;
    // printf("Address of x: %p\n", &x);
    fun(&x); //call by address
    printf("Main Value of x: %d\n",x);

    return 0;
}