/**#include<stdio.h>
void fun(int ar[], int n) //use any one: int *ar instead of ar[]
{
    for(int i=0;i<5; i++)
    {
        printf("%d ", ar[i]);
    }
}

int main (){
    int ar[5]={20, 200, 34, 50, 201};
    fun(ar, 5);
    return 0;
}*/

// using the Double data type

#include<stdio.h>
void fun(double * ar, int n) //use any one: int *ar instead of ar[]
{
    for(int i=0;i<5; i++)
    {
        printf("%0.2lf ", ar[i]);
    }
}

int main (){
    double ar[5]={20.5, 200.6, 34.4, 50, 201.675};
    fun(ar, 5);
    return 0;
}