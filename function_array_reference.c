#include<stdio.h>

//Example by integar value. work as call by reference 

/**
void fun(int * ar, int n)
{
    ar[0]=500;  //passing value of array means reference
}

int main (){
    int ar[5]={10, 20, 30, 50, 60};
    fun(ar,5);
    for(int i=0; i<5; i++)
    {
        printf("%d ",ar[i]);
    }
    return 0;
}*/

//Example by string

void fun(char * ar)
{
    ar[0]='G';  //passing value of array means reference
}

int main (){
    char ar[6]="Hello";
    fun(ar);
    printf("%s\n", ar);
    return 0;
}