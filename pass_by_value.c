#include<stdio.h>

void fun(int x)
{
    // printf("Fun address of x: %p\n", &x);
    x = 200;
}

int main (){
    int x=10;
    // printf("Main address of x: %p\n", &x);
    fun(x);
    printf("Main value of x: %d\n", x);
    
    return 0;
}