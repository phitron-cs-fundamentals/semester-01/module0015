#include<stdio.h>
int main (){
    // int x = 100;
    // int *ptr = &x;
    //Allocation Pointer address, value
    // printf("Address of x :%p\n",&x);
    // printf("Value of ptr :%p\n",ptr);
    //printf("Address of ptr :%p\n",&ptr);
    // printf("Memory of ptr size :%d\n",sizeof(ptr));

    //Ptr work as Dereferrence
    // x = 200;
    // printf("Value of x: %d\n", x);
    // printf("Value of x: %d\n", *ptr); //Dereferrence making


    double x = 5.25;
    double *ptr = &x;

    *ptr = 10.50;
    
    printf("Value of x: %0.2lf\n", x);
    printf("Value of x: %0.2lf\n", *ptr);
    printf("Memory size of x: %d\n", sizeof(&ptr));

    
    return 0;
}